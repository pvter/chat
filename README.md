# Учебный проект - чат
## Описание проекта
### Концепция
Проект представляет собой серверное приложение, позволяющее пользователям обмениваться сообщениями в реальном времени.
Приложение предоставляет API для использования веб- и мобильными приложениями, которые реализуют пользовательский интерфейс.

Для хранения учетных записей пользователей, сообщений и другой информации используется база данных. 

Для хранения, кэширования и доставки медиа файлов используется файловое хранилище и сеть CDN.

Авторизация пользователей осуществляется с использованием Google API, учетная запись привязывается к аккаунту Gmail.

### Описание предметной области и основной функционал
Сущности: Пользователь, Устройство, Чат, Сообщение, Медиа.

Пользователи могут иметь одну или несколько из ролей: пользователь, модератор, администратор. Пользователь может использовать одну учетную запись на нескольких устройствах.

Администратор может создавать, редактировать и удалять групповые чаты, редактировать роли и блокировать учетные записи пользователей, отправлять и удалять сообщения.

Модератор может блокировать учетные записи пользователей, отправлять и удалять сообщения.

Пользователи могут отправлять и получать сообщения (в групповые чаты и друг другу), редактировать свои имя и аватар, заносить пользователей в черный список.

К сообщению можно прикреплять медиа-файлы.

### Документация
- [Диаграмма ERD](./docs/ERD.pdf)
- [Диаграмма UCD](./docs/UCD.pdf)
- [Диаграмма CD](./docs/CDCD.pdf)